package com.example.crudgc.model;

import java.util.HashMap;
import java.util.Map;

public class Note {

    private String id;
    private String title;
    private String content;
    private String content1;
    private String content2;
    private String content3;
    private String content4;

    public Note() {
    }

    public Note(String id, String title, String content, String content1, String content2, String content3, String content4) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.content1 = content1;
        this.content2 = content2;
        this.content3 = content3;
        this.content4 = content4;
    }

    public Note(String title, String content, String content1, String content2, String content3, String content4) {
        this.title = title;
        this.content = content;
        this.content1 = content1;
        this.content2 = content2;
        this.content3 = content3;
        this.content4 = content4;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getContent1() {
        return content1;
    }
    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getContent2() {
        return content2;
    }
    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public String getContent3() {
        return content3;
    }
    public void setContent3(String content3) {
        this.content3 = content3;
    }

    public String getContent4() {
        return content4;
    }
    public void setContent4(String content4) {
        this.content4 = content4;
    }

    public Map<String, Object> toMap() {

        HashMap<String, Object> result = new HashMap<>();
        result.put("title", this.title);
        result.put("content", this.content);
        result.put("content1", this.content1);
        result.put("content2", this.content2);
        result.put("content3", this.content3);
        result.put("content4", this.content4);

        return result;
    }
}
