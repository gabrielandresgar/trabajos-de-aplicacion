package com.example.crudgc;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.example.crudgc.model.Note;

import java.util.Map;


public class NoteActivity extends AppCompatActivity {

    private static final String TAG = "AddNoteActivity";

    TextView edtTitle;
    TextView edtContent;
    TextView edtContent1;
    TextView edtContent2;
    TextView edtContent3;
    TextView edtContent4;
    Button btAdd;

    private FirebaseFirestore firestoreDB;
    String id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        edtTitle = findViewById(R.id.edtTitle);
        edtContent = findViewById(R.id.edtContent);
        edtContent1 = findViewById(R.id.edtContent1);
        edtContent2 = findViewById(R.id.edtContent2);
        edtContent3 = findViewById(R.id.edtContent3);
        edtContent4 = findViewById(R.id.edtContent4);
        btAdd = findViewById(R.id.btAdd);

        firestoreDB = FirebaseFirestore.getInstance();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getString("UpdateNoteId");

            edtTitle.setText(bundle.getString("UpdateNoteTitle"));
            edtContent.setText(bundle.getString("UpdateNoteContent"));
            edtContent1.setText(bundle.getString("UpdateNoteContent"));
            edtContent2.setText(bundle.getString("UpdateNoteContent"));
            edtContent3.setText(bundle.getString("UpdateNoteContent"));
            edtContent4.setText(bundle.getString("UpdateNoteContent"));
        }

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = edtTitle.getText().toString();
                String content = edtContent.getText().toString();
                String content1 = edtContent1.getText().toString();
                String content2 = edtContent2.getText().toString();
                String content3 = edtContent3.getText().toString();
                String content4 = edtContent4.getText().toString();

                if (title.length() > 0) {
                    if (id.length() > 0) {
                        updateNote(id, title, content, content1, content2, content3, content4);
                    } else {
                        addNote(title, content, content1, content2, content3, content4);
                    }
                }

                finish();
            }
        });
    }

    private void updateNote(String id, String title, String content, String content1, String content2, String content3, String content4) {
        Map<String, Object> note = (new Note(id, title, content, content1, content2, content3, content4)).toMap();

        firestoreDB.collection("notes")
                .document(id)
                .set(note)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e(TAG, "Note document update successful!");
                        Toast.makeText(getApplicationContext(), "Note has been updated!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Error adding Note document", e);
                        Toast.makeText(getApplicationContext(), "Note could not be updated!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void addNote(String title, String content, String content1, String content2, String content3, String content4) {
        Map<String, Object> note = new Note(title, content, content1, content2, content3, content4).toMap();

        firestoreDB.collection("notes")
                .add(note)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Error adding Note document", e);
                        Toast.makeText(getApplicationContext(), "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
